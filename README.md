
# Projet Quiz - Configuration et Mise en Place

## Description
Ce projet est une application de gestion de quiz développée en **Spring Boot** (API) et **React** (Client) avec une base de données **PostgreSQL**.

### Choix techniques

#### Spring Boot

Le choix de Spring Boot a été fait pour sa facilité de configuration et sa rapidité de développement. L'écosystème Spring permet de gérer facilement les dépendances, la sécurité et facilite la persistance des données. L'idée était de pouvoir se concentrer sur le développement de l'application sans se soucier de la configuration de l'API. 

Une raison importante est l'utilisation de JPA et Hibernate pour la gestion de la base de données. Cela permet de gérer facilement les entités et les relations entre les tables mais aussi les opérations CRUD nécessaires à une API REST comme celle qu'on a avec ce type de projet.


#### React

Le choix de React s'est faire sur la nécessité de réutiliser des composants et de gérer facilement l'état de l'application. React permet de créer des composants réutilisables et de gérer facilement l'état de l'application. De plus, nous avons utilisé la fonctionnalité de routing qui peut être fastidieuse en utilisant du Javascript pur.

L'utilisation de Typescript nous a permis de gérer plus facilement les types et de réduire les erreurs de développement. Typescript nous permet aussi d'utiliser des fonctionnalités plus avancées comme les interfaces ou les énumérations par exemple.    

#### Liquibase

Avec l'outil liquibase, on automatise les migrations (les changements de structure de la base de données) et a un historique des changements effectués. Cela permet de garder une trace des modifications effectuées sur la base de données et de les reproduire facilement sur un autre environnement.

#### OpenAPI 

L'utilisation d'OAS (OpenAPI Specification) s'est faite après les développements de l'API. Cela nous permet de documenter l'API dans notre cas. 


## Prérequis

Avant de commencer, assurez-vous d'avoir installé les outils suivants sur votre machine :

1. **Java** (version 21)
    - Vérifiez avec : `java --version`
2. **Maven** (version 3.6 ou plus)
    - Vérifiez avec : `mvn --version`
3. **PostgreSQL** (version 13 ou plus)
    - Vérifiez que le serveur est en cours d'exécution.
4. **Node.js** (version 14 ou plus)
5. **npm** (version 6 ou plus)
    - Vérifiez avec : `npm --version`
    - Installez avec : `npm install -g npm`

---

## Étapes d'installation (Backend)

### 1. Clonez le projet
Clonez le dépôt Git en local :
```bash
git clone <URL_DU_REPO>
cd <NOM_DU_REPO>
```

### 2. Configuration de la base de données

TODO: La connexion sera déjà setup dans le fichier `application.yml`.

### 3. Démarrage du projet

Construire le projet avec Maven :
```bash
mvn clean install
```

Lancez l'application avec Maven :
```bash
mvn spring-boot:run
```

Accédez à l'application à l'adresse suivante :
```
http://localhost:8081/api/hello
```

Vous devriez voir le message suivant :
```
Hello, World!
```

---

## Étapes d'installation (Frontend)

### 1. Installation des dépendances
Installez les dépendances du projet React :
```bash
cd quiz-client
npm install
```

### 2. Démarrage du projet

Lancez l'application React :
```bash
npm run dev
```

Accédez à l'application à l'adresse suivante :
```
http://localhost:5173
```

## Structure du projet

### Architecture

Les architectures des fichiers sont définies dans les readme respectifs des dossiers `quizz-server` et `quizz-client` [README.md](./quizz-server/README.md) et [README.md](./quizz-client/README.md).

Pour ce qui est du choix de l'architecture de notre service, nous avons opté pour une architecture en 3 couches. L'intérêt est d'abord de mieux comprendre notre application en tant que développeur et de faciliter la maintenance et l'évolution de l'application sur le long du projet. Ainsi, un changement sur une couche n'impactera que peu les autres couches. 

Dans une prochaine version, l'idée aurait été de tester notre code et ce type d'achitecture nous permet d'être indépendant de source de données et de tester facilement notre code.

(Backend)

- **`src/main/java`** : Contient le code source Java du projet.
- **`src/main/resources/db/changelog`** : Fichiers de migration Liquibase.
- **`src/main/resources/application.yml`** : Configuration de l'application.
- Retrouver le readme du backend ici : [README.md](./quizz-server/README.md)


(Frontend)

- **`quiz-client/src`** : Contient le code source React du projet.
- **`quiz-client/public`** : Fichiers statiques du projet React.
- **`quiz-client/package.json`** : Configuration du projet React.
- Retrouver le readme du frontend ici : [README.md](./quizz-client/README.md)

(Documentation)

- **`docs`** : Contient la documentation du projet. On y retrouve la spécification de l'API et des notes sur les aspects métiers du projet.

## Spécification API

La spécification de l'API sera disponible dans le fichier `docs/open-api.yaml`.

## Génération de la base de données et peuplement

La génération de la base de donnée se fait automatiquement lors du lancement de l'application. Les migrations sont gérées par Liquibase. 

## Méthodologie de gestion de projet

La gestion de projet s'est faite autour d'un simple tableau Trello. Les tâches étaient réparties en fonction des compétences de chacun et des besoins du projet. 

## Ressources utiles

1. [Documentation Liquibase](https://www.liquibase.org/documentation)
2. [Documentation Spring Boot](https://spring.io/projects/spring-boot)

Les migrations de base de données se situent dans le dossier `src/main/resources/db/changes`. Elles permettent de gérer les modifications de la base de données de manière incrémentale afin de garantir la cohérence des données sur l'évolution de l'application.

## Auteurs

Développé par Syrine, Grégoire, Ugo et Léo.