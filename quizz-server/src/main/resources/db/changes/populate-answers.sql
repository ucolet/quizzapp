-- Insert answers
INSERT INTO t_answer (answer_text, is_correct, question_id)
VALUES
    -- Question 1
    ('George Washington', TRUE, 1),
    ('Thomas Jefferson', FALSE, 1),
    ('Abraham Lincoln', FALSE, 1),
    ('John Adams', FALSE, 1),
    -- Question 2
    ('Paris', TRUE, 2),
    ('Marseille', FALSE, 2),
    ('Lyon', FALSE, 2),
    ('Toulouse', FALSE, 2),
    -- Question 3
    ('Forrest Gump', TRUE, 3),
    ('Pulp Fiction', FALSE, 3),
    ('The Shawshank Redemption', FALSE, 3),
    ('Schindler''s List', FALSE, 3),
    -- Question 4
    ('Antonio Vivaldi', TRUE, 4),
    ('Johann Sebastian Bach', FALSE, 4),
    ('Wolfgang Amadeus Mozart', FALSE, 4),
    ('Ludwig van Beethoven', FALSE, 4),
    -- Question 5
    ('11', FALSE, 5),
    ('10', TRUE, 5),
    ('9', FALSE, 5),
    ('12', FALSE, 5),
    -- Question 6
    ('Madrid', TRUE, 6),
    ('Barcelone', FALSE, 6),
    ('Valence', FALSE, 6),
    ('Séville', FALSE, 6),
    -- Question 7
    ('Braveheart', TRUE, 7),
    ('Le Patient Anglais', FALSE, 7),
    ('La Ligne Verte', FALSE, 7),
    ('Schindler''s List', FALSE, 7),
    -- Question 8
    ('Ludwig van Beethoven', TRUE, 8),
    ('Johann Sebastian Bach', FALSE, 8),
    ('Wolfgang Amadeus Mozart', FALSE, 8),
    ('Antonio Vivaldi', FALSE, 8),
    -- Question 9
    ('5', FALSE, 9),
    ('6', FALSE, 9),
    ('10', TRUE, 9),
    ('8', FALSE, 9),
    -- Question 10
    ('Victor Hugo', TRUE, 10),
    ('Gustave Flaubert', FALSE, 10),
    ('Émile Zola', FALSE, 10),
    ('Honoré de Balzac', FALSE, 10),
    -- Question 11
    ('Rome', TRUE, 11),
    ('Milan', FALSE, 11),
    ('Naples', FALSE, 11),
    ('Venise', FALSE, 11),
    -- Question 12
    ('The English Patient', TRUE, 12),
    ('Braveheart', FALSE, 12),
    ('The Green Mile', FALSE, 12),
    ('Schindler''s List', FALSE, 12),
    -- Question 13
    ('Ludwig van Beethoven', TRUE, 13),
    ('Johann Sebastian Bach', FALSE, 13),
    ('Wolfgang Amadeus Mozart', FALSE, 13),
    ('Antonio Vivaldi', FALSE, 13),
    -- Question 14
    ('15', FALSE, 14),
    ('16', FALSE, 14),
    ('17', TRUE, 14),
    ('18', FALSE, 14),
    -- Question 15
    ('Stendhal', TRUE, 15),
    ('Gustave Flaubert', FALSE, 15),
    ('Victor Hugo', FALSE, 15),
    ('Honoré de Balzac', FALSE, 15),
    -- Question 16
    ('Berlin', TRUE, 16),
    ('Munich', FALSE, 16),
    ('Francfort', FALSE, 16),
    ('Hambourg', FALSE, 16),
    -- Question 17
    ('Titanic', TRUE, 17),
    ('The Green Mile', FALSE, 17),
    ('The Matrix', FALSE, 17),
    ('Saving Private Ryan', FALSE, 17),
    -- Question 18
    ('Ludwig van Beethoven', TRUE, 18),
    ('Johann Sebastian Bach', FALSE, 18),
    ('Wolfgang Amadeus Mozart', FALSE, 18),
    ('Antonio Vivaldi', FALSE, 18),
    -- Question 19
    ('7', TRUE, 19),
    ('6', FALSE, 19),
    ('8', FALSE, 19),
    ('9', FALSE, 19),
    -- Question 20
    ('Gustave Flaubert', TRUE, 20),
    ('Victor Hugo', FALSE, 20),
    ('Émile Zola', FALSE, 20),
    ('Honoré de Balzac', FALSE, 20),
    -- Question 21
    ('Londres', TRUE, 21),
    ('Édimbourg', FALSE, 21),
    ('Cardiff', FALSE, 21),
    ('Dublin', FALSE, 21),
    -- Question 22
    ('Les Nuits Fauves', TRUE, 22),
    ('Germinal', FALSE, 22),
    ('Amélie Poulain', FALSE, 22),
    ('Les Choristes', FALSE, 22),
    -- Question 23
    ('Ludwig van Beethoven', TRUE, 23),
    ('Johann Sebastian Bach', FALSE, 23),
    ('Wolfgang Amadeus Mozart', FALSE, 23),
    ('Antonio Vivaldi', FALSE, 23),
    -- Question 24
    ('6', TRUE, 24),
    ('5', FALSE, 24),
    ('7', FALSE, 24),
    ('8', FALSE, 24),
    -- Question 25
    ('Alexandre Dumas', TRUE, 25),
    ('Victor Hugo', FALSE, 25),
    ('Émile Zola', FALSE, 25),
    ('Honoré de Balzac', FALSE, 25);
