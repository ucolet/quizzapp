-- Insert questions
INSERT INTO t_question (question_text, difficulty_level, theme, points, penalty)
VALUES
    ('Qui était le premier président des États-Unis ?', 'EASY', 'HISTORY', 10, 5),
    ('Quelle est la capitale de la France ?', 'EASY', 'HISTORY', 10, 5),
    ('Quel film a remporté l''Oscar du meilleur film en 1994 ?', 'MEDIUM', 'MOVIES', 20, 10),
    ('Qui a composé les Quatre Saisons ?', 'MEDIUM', 'MUSIC', 20, 10),
    ('Combien de joueurs y a-t-il dans une équipe de football ?', 'HARD', 'SPORTS', 30, 15),
-- generate 20 more different questions
    ('Quelle est la capitale de l''Espagne ?', 'EASY', 'HISTORY', 10, 5),
    ('Quel film a remporté l''Oscar du meilleur film en 1995 ?', 'MEDIUM', 'MOVIES', 20, 10),
    ('Qui a composé la 9ème symphonie ?', 'MEDIUM', 'MUSIC', 20, 10),
    ('Combien de joueurs y a-t-il dans une équipe de basket ?', 'HARD', 'SPORTS', 30, 15),
    ('Qui a écrit le roman "Les Misérables" ?', 'HARD', 'LITERATURE', 30, 15),
    ('Quelle est la capitale de l''Italie ?', 'EASY', 'HISTORY', 10, 5),
    ('Quel film a remporté l''Oscar du meilleur film en 1996 ?', 'MEDIUM', 'MOVIES', 20, 10),
    ('Qui a composé la 5ème symphonie ?', 'MEDIUM', 'MUSIC', 20, 10),
    ('Combien de joueurs y a-t-il dans une équipe de rugby ?', 'HARD', 'SPORTS', 30, 15),
    ('Qui a écrit le roman "Le Rouge et le Noir" ?', 'HARD', 'LITERATURE', 30, 15),
    ('Quelle est la capitale de l''Allemagne ?', 'EASY', 'HISTORY', 10, 5),
    ('Quel film a remporté l''Oscar du meilleur film en 1997 ?', 'MEDIUM', 'MOVIES', 20, 10),
    ('Qui a composé la 6ème symphonie ?', 'MEDIUM', 'MUSIC', 20, 10),
    ('Combien de joueurs y a-t-il dans une équipe de handball ?', 'HARD', 'SPORTS', 30, 15),
    ('Qui a écrit le roman "Madame Bovary" ?', 'HARD', 'LITERATURE', 30, 15),
    ('Quelle est la capitale du Royaume-Uni ?', 'EASY', 'HISTORY', 10, 5),
    ('Quel film a remporté le César du meilleur film en 1994 ?', 'MEDIUM', 'MOVIES', 20, 10),
    ('Qui a composé la 7ème symphonie ?', 'MEDIUM', 'MUSIC', 20, 10),
    ('Combien de joueurs y a-t-il dans une équipe de volley-ball ?', 'HARD', 'SPORTS', 30, 15),
    ('Qui a écrit le roman "Les Trois Mousquetaires" ?', 'HARD', 'LITERATURE', 30, 15);
