package org.quizz.server.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.quizz.server.dto.GameDTO;
import org.quizz.server.entity.Game;

@Mapper(componentModel = "spring")
public interface GameMapper {

    Game toEntity(GameDTO gameDTO);
    @Mapping(target = "teams", source = "teams" )
    GameDTO toDTO(Game game);
}
