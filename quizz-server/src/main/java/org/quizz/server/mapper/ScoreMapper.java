package org.quizz.server.mapper;

import org.mapstruct.Mapping;
import org.quizz.server.dto.ScoreDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.quizz.server.entity.Score;

@Mapper(componentModel = "spring")
public interface ScoreMapper {
    @Mapping(
            target = "game",
            source = "game"
    )
    Score toEntity(ScoreDTO scoreDTO);
    ScoreDTO toDTO(Score score);
}
