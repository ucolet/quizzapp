package org.quizz.server.mapper;

import org.mapstruct.Mapper;
import org.quizz.server.dto.AnswerDTO;
import org.quizz.server.entity.Answer;

@Mapper(componentModel = "spring")
public interface AnswerMapper {
    Answer toEntity(AnswerDTO answerDTO);
    AnswerDTO toDTO(Answer answer);
}
