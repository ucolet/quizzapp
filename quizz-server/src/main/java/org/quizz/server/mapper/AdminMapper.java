package org.quizz.server.mapper;

import org.quizz.server.dto.AdminDTO;
import org.mapstruct.Mapper;
import org.quizz.server.entity.Admin;

@Mapper(componentModel = "spring")
public interface AdminMapper {
    Admin toEntity(AdminDTO administrationDTO);
    AdminDTO toDTO(Admin administration);
}
