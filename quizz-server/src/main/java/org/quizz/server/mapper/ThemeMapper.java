package org.quizz.server.mapper;

import org.quizz.server.dto.ThemeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.quizz.server.entity.Theme;

@Mapper(componentModel = "spring")
public interface ThemeMapper {
    Theme toEntity(ThemeDTO themeDTO);
    ThemeDTO toDTO(Theme theme);
}
