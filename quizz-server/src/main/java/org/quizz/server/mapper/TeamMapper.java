package org.quizz.server.mapper;

import org.quizz.server.dto.TeamDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.quizz.server.entity.Team;

@Mapper(componentModel = "spring")
public interface TeamMapper {

    Team toEntity(TeamDTO teamDTO);
    TeamDTO toDTO(Team team);
}
