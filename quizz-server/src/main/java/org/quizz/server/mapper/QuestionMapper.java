package org.quizz.server.mapper;

import org.mapstruct.Mapper;
import org.quizz.server.dto.QuestionDTO;
import org.quizz.server.entity.Question;

@Mapper(componentModel = "spring")
public interface QuestionMapper {
    Question toEntity(QuestionDTO questionDTO);
    QuestionDTO toDTO(Question question);
}
