package org.quizz.server.mapper;

import org.quizz.server.dto.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.quizz.server.entity.User;

@Mapper(componentModel = "spring")
public interface UserMapper {


    User toEntity(UserDTO userDTO);
    UserDTO toDTO(User user);
}
