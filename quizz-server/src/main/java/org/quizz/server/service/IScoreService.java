package org.quizz.server.service;

import org.quizz.server.entity.Score;

import java.util.List;
import java.util.Optional;

public interface IScoreService {
    Score createScore(Score score);
    Optional<Score> getScoreById(Long scoreId);
    List<Score> getAllScores();
    List<Score> getScoresByUserId(int userId);
}
