package org.quizz.server.service.impl;

import org.quizz.server.entity.Admin;
import org.quizz.server.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.quizz.server.service.IAdminService;

import java.util.List;
import java.util.Optional;

@Service
public class AdminServiceImpl implements IAdminService {
    @Autowired
    private AdminRepository adminRepository;

    @Override
    public Admin createAction(Admin administration) {
        return adminRepository.save(administration);
    }

    @Override
    public Optional<Admin> getActionById(Long adminActionId) {
        return adminRepository.findById(adminActionId);
    }

    @Override
    public List<Admin> getAllActions() {
        return adminRepository.findAll();
    }
}
