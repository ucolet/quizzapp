package org.quizz.server.service;

import org.quizz.server.entity.Answer;

import java.util.List;
import java.util.Optional;

public interface IAnswerService {
    Answer createAnswer(Answer answer);
    Optional<Answer> getAnswerById(Long answerId);
    List<Answer> getAllAnswers();
}
