package org.quizz.server.service;

import org.quizz.server.entity.Theme;

import java.util.List;
import java.util.Optional;

public interface IThemeService {
    Theme createTheme(Theme theme);
    Optional<Theme> getThemeById(Long themeId);
    List<Theme> getAllThemes();
}
