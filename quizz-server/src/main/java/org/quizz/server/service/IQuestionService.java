package org.quizz.server.service;

import org.quizz.server.entity.Question;

import java.util.List;

public interface IQuestionService {

    List<Question> getRandomQuestions(String theme, String difficultyLevel, int limit);

    Question createQuestion(Question entity);

    Question getQuestionById(Long questionId) throws Exception;

    List<Question> getAllQuestions();
}
