package org.quizz.server.service;

import org.quizz.server.entity.Admin;

import java.util.List;
import java.util.Optional;

public interface IAdminService {
    Admin createAction(Admin administration);
    Optional<Admin> getActionById(Long adminActionId);
    List<Admin> getAllActions();
}
