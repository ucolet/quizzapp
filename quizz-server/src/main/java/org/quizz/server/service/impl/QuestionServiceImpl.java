package org.quizz.server.service.impl;

import org.quizz.server.entity.Question;
import org.quizz.server.repository.QuestionRepository;
import org.quizz.server.service.IQuestionService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImpl implements IQuestionService {

    private final QuestionRepository questionRepository;

    public QuestionServiceImpl(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    public List<Question> getRandomQuestions(String theme, String difficultyLevel, int limit) {
        return questionRepository.findRandomQuestions(theme, difficultyLevel, limit);
    }

    @Override
    public Question createQuestion(Question entity) {
        return questionRepository.save(entity);
    }

    @Override
    public Question getQuestionById(Long questionId) throws Exception {
        return questionRepository.findById(questionId).orElseThrow(() -> new Exception("Question not found"));
    }

    @Override
    public List<Question> getAllQuestions() {
        return questionRepository.findAll();
    }
}
