package org.quizz.server.service.impl;

import org.quizz.server.entity.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.quizz.server.repository.TeamRepository;
import org.quizz.server.service.ITeamService;

import java.util.List;
import java.util.Optional;

@Service
public class TeamServiceImpl implements ITeamService {
    @Autowired
    private TeamRepository teamRepository;

    @Override
    public Team createTeam(Team team) {
        return teamRepository.save(team);
    }

    @Override
    public Optional<Team> getTeamById(Long teamId) {
        return teamRepository.findById(teamId);
    }

    @Override
    public List<Team> getAllTeams() {
        return teamRepository.findAll();
    }
}
