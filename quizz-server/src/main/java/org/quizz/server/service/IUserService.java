package org.quizz.server.service;

import org.quizz.server.entity.User;

import java.util.List;

public interface IUserService {
    User createUser(User user);
    User getUserById(Long userId);
    List<User> getAllUsers();
}
