package org.quizz.server.service.impl;

import jakarta.persistence.EntityNotFoundException;
import org.quizz.server.entity.Game;
import org.quizz.server.entity.Score;
import org.quizz.server.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.quizz.server.repository.ScoreRepository;
import org.quizz.server.service.IScoreService;

import java.util.List;
import java.util.Optional;

@Service
public class ScoreServiceImpl implements IScoreService {
    @Autowired
    private ScoreRepository scoreRepository;

    @Autowired
    private GameRepository gameRepository;

    @Override
    public Score createScore(Score score) {

        // Vérifie l'existence du jeu
        Optional<Game> gameOpt = gameRepository.findByGameId(score.getGame().getGameId());

        if (gameOpt.isEmpty()) {
            // Si le jeu n'existe pas, retourner null ou lancer une exception
            throw new EntityNotFoundException("Le jeu avec l'ID " + score.getGame().getGameId() + " n'existe pas.");
        }

        // Le jeu existe donc on peut créer le score
        return scoreRepository.save(score);
    }

    @Override
    public Optional<Score> getScoreById(Long scoreId) {
        return scoreRepository.findById(scoreId);
    }

    @Override
    public List<Score> getAllScores() {
        return scoreRepository.findAll();
    }

    @Override
    public List<Score> getScoresByUserId(int userId) { return scoreRepository.findByUserUserId(userId); }
}
