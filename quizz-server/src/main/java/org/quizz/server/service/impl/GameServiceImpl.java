package org.quizz.server.service.impl;

import org.quizz.server.dto.GameSoloCreatedDTO;
import org.quizz.server.dto.GameMultiCreatedDTO;
import org.quizz.server.entity.Game;
import org.quizz.server.entity.Question;
import org.quizz.server.service.IGameService;
import org.quizz.server.service.IQuestionService;
import org.springframework.stereotype.Service;
import org.quizz.server.repository.GameRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class GameServiceImpl implements IGameService {

    private final GameRepository gameRepository;
    private final IQuestionService questionService;

    public GameServiceImpl(GameRepository gameRepository, IQuestionService questionService) {
        this.gameRepository = gameRepository;
        this.questionService = questionService;
    }

    @Override
    public GameSoloCreatedDTO createSoloGame(Game game) {
        // Generer un set de questions en fonction des paramètres reçus
        // Les questions doivent être sélectionnées en fonction du thème, de la difficulté et du nombre de questions
        // Créer une partie avec les questions générées et leurs réponses
        game.setGameDate(new Date());
        Game GameSoloCreated = gameRepository.save(game);
        return new GameSoloCreatedDTO(
                GameSoloCreated,
                questionService.getRandomQuestions(game.getTheme(), game.getDifficulty(), game.getQuestionCount())
        );
    }

    @Override
    public GameMultiCreatedDTO createMultiGame(Game game) {
        if (!"MULTI".equalsIgnoreCase(game.getMode())) {
            throw new IllegalArgumentException("Ce service est uniquement pour les parties par équipe.");
        }
        game.setGameDate(new Date());
        Game gameMultiCreated = gameRepository.save(game);
        GameMultiCreatedDTO gameMultiCreatedDTO = null;

        List<Question> allQuestions = questionService.getRandomQuestions(
                game.getTheme(), game.getDifficulty(), game.getQuestionCount()
        );


        if ("VERSION1".equalsIgnoreCase(game.getVersion())) {
            // Divise les questions en deux listes pour les équipes
            List<Question> teamAQuestions = allQuestions.subList(0, allQuestions.size() / 2);
            List<Question> teamBQuestions = allQuestions.subList(allQuestions.size() / 2, allQuestions.size());
            gameMultiCreatedDTO = new GameMultiCreatedDTO(gameMultiCreated , teamAQuestions, teamBQuestions);
        } else if ("VERSION2".equalsIgnoreCase(game.getVersion())) {
            // Utilise une seule liste de questions partagée
            gameMultiCreatedDTO = new GameMultiCreatedDTO(gameMultiCreated, allQuestions, allQuestions);
        }

        return gameMultiCreatedDTO;
    }


    @Override
    public Optional<Game> getGameById(Long gameId) {
        return gameRepository.findById(gameId);
    }

    @Override
    public List<Game> getAllGames() {
        return gameRepository.findAll();
    }

    @Override
    public Game updateGame(Long gameId, Game entity) {
        Optional<Game> game = gameRepository.findById(gameId);
        if (game.isPresent()) {
            Game gameToUpdate = game.get();
            gameToUpdate.setGameDate(entity.getGameDate());
            gameToUpdate.setMode(entity.getMode());
            gameToUpdate.setDifficulty(entity.getDifficulty());
            gameToUpdate.setQuestionCount(entity.getQuestionCount());
            gameToUpdate.setTheme(entity.getTheme());
            gameToUpdate.setStatus(entity.getStatus());
            return gameRepository.save(gameToUpdate);
        }
        return null;
    }
}
