package org.quizz.server.service;

import org.quizz.server.dto.GameMultiCreatedDTO;
import org.quizz.server.dto.GameSoloCreatedDTO;
import org.quizz.server.entity.Game;

import java.util.List;
import java.util.Optional;

public interface IGameService {
    GameSoloCreatedDTO createSoloGame(Game game);
    GameMultiCreatedDTO createMultiGame(Game game);
    Optional<Game> getGameById(Long gameId);
    List<Game> getAllGames();
    Game updateGame(Long gameId, Game entity);
}
