package org.quizz.server.service.impl;

import org.quizz.server.entity.Theme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.quizz.server.repository.ThemeRepository;
import org.quizz.server.service.IThemeService;

import java.util.List;
import java.util.Optional;

@Service
public class ThemeServiceImpl implements IThemeService {
    @Autowired
    private ThemeRepository themeRepository;

    @Override
    public Theme createTheme(Theme theme) {
        return themeRepository.save(theme);
    }

    @Override
    public Optional<Theme> getThemeById(Long themeId) {
        return themeRepository.findById(themeId);
    }

    @Override
    public List<Theme> getAllThemes() {
        return themeRepository.findAll();
    }
}
