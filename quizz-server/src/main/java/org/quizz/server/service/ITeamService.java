package org.quizz.server.service;

import org.quizz.server.entity.Team;

import java.util.List;
import java.util.Optional;

public interface ITeamService {
    Team createTeam(Team team);
    Optional<Team> getTeamById(Long teamId);
    List<Team> getAllTeams();
}
