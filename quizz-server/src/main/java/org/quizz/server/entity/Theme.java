package org.quizz.server.entity;

import lombok.Data;

import jakarta.persistence.*;

@Entity(name = "t_theme")
@Data
public class Theme {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTheme;

    private String name;
}
