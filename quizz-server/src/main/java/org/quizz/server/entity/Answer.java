package org.quizz.server.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import jakarta.persistence.*;

@Entity(name = "t_answer")
@Data
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int answerId;

    @ManyToOne
    @JoinColumn(name = "question_id", nullable = false)
    @JsonIgnore // Prevents recursive serialization
    private Question question;

    private String answerText;
    private boolean isCorrect;
}
