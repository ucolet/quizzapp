package org.quizz.server.entity;

import lombok.Data;

import jakarta.persistence.*;
import java.util.List;

@Entity(name = "t_user")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userId;
    private String username;
    @Column(columnDefinition = "TEXT")
    private String profilePicture;
    private String password;
    private String role;
    private String accountStatus;

    @OneToMany(mappedBy = "user")
    private List<Score> scores;

    @OneToMany(mappedBy = "admin")
    private List<Admin> actions;
}
