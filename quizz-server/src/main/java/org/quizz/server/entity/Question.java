package org.quizz.server.entity;

import lombok.Data;

import jakarta.persistence.*;
import java.util.List;

@Entity(name = "t_question")
@Data
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int questionId;

    private String questionText;
    private String difficultyLevel;
    private String theme;
    private int points;
    private int penalty;

    @OneToMany(mappedBy = "question")
    private List<Answer> answers;
}
