package org.quizz.server.entity;

import lombok.Data;

import jakarta.persistence.*;
import java.util.List;

@Entity(name = "t_team")
@Data
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int teamId;

    private String teamName;

    @ManyToOne
    @JoinColumn(name = "game_id", nullable = false)
    private Game game;

    @OneToMany(mappedBy = "team")
    private List<Score> scores;
}
