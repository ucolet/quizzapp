package org.quizz.server.entity;

import lombok.Data;

import jakarta.persistence.*;
import java.util.Date;
import java.util.List;

@Entity(name = "t_game")
@Data
//TODO: Enlever duration du script SQL
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int gameId;
    private Long organizerId;

    @Temporal(TemporalType.DATE)
    private Date gameDate;

    private String mode;


    private String difficulty;
    private int questionCount;
    private String theme;
    private String version;
    private String status;

    @OneToMany(mappedBy = "game")
    private List<Team> teams;

    @OneToMany(mappedBy = "game")
    private List<Score> scores;
}
