package org.quizz.server.entity;

import lombok.Data;

import jakarta.persistence.*;
import java.util.Date;

@Entity(name = "t_administration")
@Data
public class Admin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int adminActionId;

    @ManyToOne
    @JoinColumn(name = "admin_id", nullable = false)
    private User admin;

    private String action;

    @Temporal(TemporalType.DATE)
    private Date actionDate;
}
