package org.quizz.server.repository;

import org.aspectj.weaver.patterns.TypePatternQuestions;
import org.quizz.server.entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

    @Query("SELECT q FROM t_question q WHERE q.theme = :theme AND q.difficultyLevel = :difficultyLevel")
    List<TypePatternQuestions.Question> findQuestionsByThemeAndDifficulty(
            @Param("theme") String theme,
            @Param("difficultyLevel") String difficultyLevel
    );

    @Query(value = "SELECT * FROM t_question WHERE theme = :theme AND difficulty_level = :difficultyLevel ORDER BY RAND() LIMIT :limit", nativeQuery = true)
    List<Question> findRandomQuestions(
            @Param("theme") String theme,
            @Param("difficultyLevel") String difficultyLevel,
            @Param("limit") int limit
    );
}
