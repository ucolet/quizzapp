package org.quizz.server.repository;

import org.quizz.server.entity.Score;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScoreRepository extends JpaRepository<Score, Long> {
    List<Score> findByUserUserId(int userId); // Méthode pour récupérer les scores d'un utilisateur

}
