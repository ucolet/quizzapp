package org.quizz.server.controller;

import org.quizz.server.entity.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.quizz.server.dto.AnswerDTO;
import org.quizz.server.service.IAnswerService;
import org.quizz.server.mapper.AnswerMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.stream.Collectors;

// INFO: Sert à pouvoir créer des réponses liées à des questions lorsqu'on est admin.
// WARN: Ne pas oublier qu'une réponse peut s'accompagner d'un malus.

@RestController
@RequestMapping("/api/answers")
public class AnswerController {

    private final IAnswerService answerService;
    private final AnswerMapper answerMapper;

    public AnswerController(IAnswerService answerService, AnswerMapper answerMapper) {
        this.answerService = answerService;
        this.answerMapper = answerMapper;
    }

    @PostMapping
    public ResponseEntity<AnswerDTO> createAnswer(@RequestBody AnswerDTO answerDTO) {
        Answer answer = answerService.createAnswer(answerMapper.toEntity(answerDTO));
        return new ResponseEntity<>(answerMapper.toDTO(answer), HttpStatus.CREATED);
    }

    @GetMapping("/{answerId}")
    public ResponseEntity<AnswerDTO> getAnswerById(@PathVariable Long answerId) {
        Answer answer = answerService.getAnswerById(answerId).orElseThrow(() -> new RuntimeException("Answer not found"));
        return new ResponseEntity<>(answerMapper.toDTO(answer), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<AnswerDTO>> getAllAnswers() {
        List<Answer> answers = answerService.getAllAnswers();
        List<AnswerDTO> answerDTOs = answers.stream().map(answerMapper::toDTO).collect(Collectors.toList());
        return new ResponseEntity<>(answerDTOs, HttpStatus.OK);
    }
}
