package org.quizz.server.controller;

import org.quizz.server.entity.Theme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.quizz.server.dto.ThemeDTO;
import org.quizz.server.service.IThemeService;
import org.quizz.server.mapper.ThemeMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/themes")
public class ThemeController {


    private final IThemeService themeService;
    private final ThemeMapper themeMapper;

    public ThemeController(IThemeService themeService, ThemeMapper themeMapper) {
        this.themeService = themeService;
        this.themeMapper = themeMapper;
    }

    @PostMapping
    public ResponseEntity<ThemeDTO> createTheme(@RequestBody ThemeDTO themeDTO) {
        Theme theme = themeService.createTheme(themeMapper.toEntity(themeDTO));
        return new ResponseEntity<>(themeMapper.toDTO(theme), HttpStatus.CREATED);
    }

    @GetMapping("/{themeId}")
    public ResponseEntity<ThemeDTO> getThemeById(@PathVariable Long themeId) {
        Theme theme = themeService.getThemeById(themeId).orElseThrow(() -> new RuntimeException("Theme not found"));
        return new ResponseEntity<>(themeMapper.toDTO(theme), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<ThemeDTO>> getAllThemes() {
        List<Theme> themes = themeService.getAllThemes();
        List<ThemeDTO> themeDTOs = themes.stream().map(themeMapper::toDTO).collect(Collectors.toList());
        return new ResponseEntity<>(themeDTOs, HttpStatus.OK);
    }
}
