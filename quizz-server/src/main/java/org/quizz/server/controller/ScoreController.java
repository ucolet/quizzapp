package org.quizz.server.controller;

import org.quizz.server.entity.Score;
import org.quizz.server.service.impl.ScoreServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.quizz.server.dto.ScoreDTO;
import org.quizz.server.service.IScoreService;
import org.quizz.server.mapper.ScoreMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.stream.Collectors;

// TODO: La création d'un score se fait lorsque la partie est terminée. Le score est reçu depuis le client qui s'occupe du calcul de celui-ci à mesure que la partie avance. On retourne un objet ScoreDTO avec l'id nouvellement créé et le score.

@RestController
@RequestMapping("/api/scores")
public class ScoreController {

    private final IScoreService scoreService;
    private final ScoreMapper scoreMapper;
    private final Logger logger = LoggerFactory.getLogger(ScoreController.class);


    public ScoreController(IScoreService scoreService, ScoreMapper scoreMapper) {
        this.scoreService = scoreService;
        this.scoreMapper = scoreMapper;
    }

    @PostMapping
    public ResponseEntity<ScoreDTO> createScore(@RequestBody ScoreDTO scoreDTO) {
        Score score = scoreService.createScore(scoreMapper.toEntity(scoreDTO));
        return new ResponseEntity<>(scoreMapper.toDTO(score), HttpStatus.CREATED);
    }

    @GetMapping("/{scoreId}")
    public ResponseEntity<ScoreDTO> getScoreById(@PathVariable Long scoreId) {
        Score score = scoreService.getScoreById(scoreId).orElseThrow(() -> new RuntimeException("Score not found"));
        return new ResponseEntity<>(scoreMapper.toDTO(score), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<ScoreDTO>> getAllScores() {
        List<Score> scores = scoreService.getAllScores();
        List<ScoreDTO> scoreDTOs = scores.stream().map(scoreMapper::toDTO).collect(Collectors.toList());
        return new ResponseEntity<>(scoreDTOs, HttpStatus.OK);
    }

    @GetMapping("/users/{userId}/scores")
    public ResponseEntity<List<ScoreDTO>> getScoresByUserId(@PathVariable int userId) {
        logger.info("Récupération des scores pour l'utilisateur avec ID: {}", userId);

        List<Score> scores = scoreService.getScoresByUserId(userId);
        if (scores.isEmpty()) {
            logger.info("Aucun score trouvé pour l'utilisateur avec ID: {}", userId);
            return ResponseEntity.noContent().build();// Si aucun score n'est trouvé
        }

        logger.info("Nombre de scores trouvés pour l'utilisateur avec ID: {}: {}", userId, scores.size());
        List<ScoreDTO> scoreDTOs = scores.stream().map(scoreMapper::toDTO).collect(Collectors.toList());
        return new ResponseEntity<>(scoreDTOs, HttpStatus.OK); // Retourne les scores de l'utilisateur
    }


}
