package org.quizz.server.controller;

import org.quizz.server.dto.GameMultiCreatedDTO;
import org.quizz.server.dto.GameSoloCreatedDTO;
import org.quizz.server.entity.Game;
import org.springframework.web.bind.annotation.*;
import org.quizz.server.dto.GameDTO;
import org.quizz.server.service.IGameService;
import org.quizz.server.mapper.GameMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.stream.Collectors;

// TODO: Lors de la création d'une partie (POST /api/games), le service doit générer un set de questions en fonction des paramètres reçus. Les questions doivent être sélectionnées en fonction du thème, de la difficulté et du nombre de questions. Le service doit ensuite créer une partie avec les questions générées et leurs réponses. Ce qu'on retourne est donc un objet GameDTO avec son id nouvellement créé et les questions générées + réponses à ces questions (GameSoloCreatedDTO à faire ?)
// WARN: Le mode de jeu est SOLO ou EQUIPE et dans le cas d'un EQUIPE, on génère une liste de questions ou une pour chaque équipe. Il faut donc bien le prendre en compte dans le GameService pour générer les réponses.

@RestController
@CrossOrigin(origins = "http://localhost:5173")
@RequestMapping("/api/games")
public class GameController {

    private final IGameService gameService;
    private final GameMapper gameMapper;

    public GameController(IGameService gameService, GameMapper gameMapper) {
        this.gameService = gameService;
        this.gameMapper = gameMapper;
    }

    @PostMapping("/solo")
    public ResponseEntity<GameSoloCreatedDTO> createSoloGame(@RequestBody GameDTO gameDTO) {
        GameSoloCreatedDTO GameSoloCreated = gameService.createSoloGame(gameMapper.toEntity(gameDTO));
        return new ResponseEntity<>(GameSoloCreated, HttpStatus.CREATED);
    }

    @PostMapping("/multi")
    public ResponseEntity<GameMultiCreatedDTO> createMultiGame(@RequestBody GameDTO gameDTO) {
        GameMultiCreatedDTO GameMultiCreated = gameService.createMultiGame(gameMapper.toEntity(gameDTO));
        return new ResponseEntity<>(GameMultiCreated, HttpStatus.CREATED);
    }

    @GetMapping("/{gameId}")
    public ResponseEntity<GameDTO> getGameById(@PathVariable Long gameId) {
        Game game = gameService.getGameById(gameId).orElseThrow(() -> new RuntimeException("Game not found"));
        return new ResponseEntity<>(gameMapper.toDTO(game), HttpStatus.OK);
    }

    @PatchMapping("/{gameId}")
    public ResponseEntity<GameDTO> updateGame(@PathVariable Long gameId, @RequestBody GameDTO gameDTO) {
        Game game = gameService.updateGame(gameId, gameMapper.toEntity(gameDTO));
        return new ResponseEntity<>(gameMapper.toDTO(game), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<GameDTO>> getAllGames() {
        List<Game> games = gameService.getAllGames();
        List<GameDTO> gameDTOs = games.stream().map(gameMapper::toDTO).collect(Collectors.toList());
        return new ResponseEntity<>(gameDTOs, HttpStatus.OK);
    }
}
