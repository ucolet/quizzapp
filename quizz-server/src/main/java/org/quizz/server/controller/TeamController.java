package org.quizz.server.controller;

import org.quizz.server.entity.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.quizz.server.dto.TeamDTO;
import org.quizz.server.service.ITeamService;
import org.quizz.server.mapper.TeamMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/teams")
public class TeamController {

    private final ITeamService teamService;
    private final TeamMapper teamMapper;

    public TeamController(ITeamService teamService, TeamMapper teamMapper) {
        this.teamService = teamService;
        this.teamMapper = teamMapper;
    }

    @PostMapping
    public ResponseEntity<TeamDTO> createTeam(@RequestBody TeamDTO teamDTO) {
        Team team = teamService.createTeam(teamMapper.toEntity(teamDTO));
        return new ResponseEntity<>(teamMapper.toDTO(team), HttpStatus.CREATED);
    }

    @GetMapping("/{teamId}")
    public ResponseEntity<TeamDTO> getTeamById(@PathVariable Long teamId) {
        Team team = teamService.getTeamById(teamId).orElseThrow(() -> new RuntimeException("Team not found"));
        return new ResponseEntity<>(teamMapper.toDTO(team), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<TeamDTO>> getAllTeams() {
        List<Team> teams = teamService.getAllTeams();
        List<TeamDTO> teamDTOs = teams.stream().map(teamMapper::toDTO).collect(Collectors.toList());
        return new ResponseEntity<>(teamDTOs, HttpStatus.OK);
    }
}
