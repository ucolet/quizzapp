package org.quizz.server.controller;

import org.quizz.server.entity.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.quizz.server.dto.QuestionDTO;
import org.quizz.server.service.IQuestionService;
import org.quizz.server.mapper.QuestionMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/questions")
public class QuestionController {


    private final IQuestionService questionService;
    private final QuestionMapper questionMapper;

    public QuestionController(IQuestionService questionService, QuestionMapper questionMapper) {
        this.questionService = questionService;
        this.questionMapper = questionMapper;
    }

    @PostMapping
    public ResponseEntity<QuestionDTO> createQuestion(@RequestBody QuestionDTO questionDTO) {
        Question question = questionService.createQuestion(questionMapper.toEntity(questionDTO));
        return new ResponseEntity<>(questionMapper.toDTO(question), HttpStatus.CREATED);
    }

    @GetMapping("/{questionId}")
    public ResponseEntity<QuestionDTO> getQuestionById(@PathVariable Long questionId) throws Exception {
        Question question = questionService.getQuestionById(questionId);
        return new ResponseEntity<>(questionMapper.toDTO(question), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<QuestionDTO>> getAllQuestions() {
        List<Question> questions = questionService.getAllQuestions();
        List<QuestionDTO> questionDTOs = getQuestionDTOs(questions);
        return new ResponseEntity<>(questionDTOs, HttpStatus.OK);
    }

    private List<QuestionDTO> getQuestionDTOs(List<Question> questions) {
        List<QuestionDTO> questionDTOS = new ArrayList<>();
        questions.forEach(question -> {
            questionDTOS.add(new QuestionDTO(question));
        });
        return questionDTOS;
    }
};
