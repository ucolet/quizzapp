package org.quizz.server.controller;

import org.quizz.server.entity.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.quizz.server.dto.AdminDTO;
import org.quizz.server.service.IAdminService;
import org.quizz.server.mapper.AdminMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/admin")
public class AdminController {


    private final IAdminService administrationService;
    private final AdminMapper administrationMapper;

    public AdminController(IAdminService administrationService, AdminMapper administrationMapper) {
        this.administrationService = administrationService;
        this.administrationMapper = administrationMapper;
    }

    @PostMapping
    public ResponseEntity<AdminDTO> createAdminAction(@RequestBody AdminDTO administrationDTO) {
        Admin administration = administrationService.createAction(administrationMapper.toEntity(administrationDTO));
        return new ResponseEntity<>(administrationMapper.toDTO(administration), HttpStatus.CREATED);
    }

    @GetMapping("/{adminActionId}")
    public ResponseEntity<AdminDTO> getAdminActionById(@PathVariable Long adminActionId) {
        Admin administration = administrationService.getActionById(adminActionId).orElseThrow(() -> new RuntimeException("Action not found"));
        return new ResponseEntity<>(administrationMapper.toDTO(administration), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<AdminDTO>> getAllAdminActions() {
        List<Admin> actions = administrationService.getAllActions();
        List<AdminDTO> actionDTOs = actions.stream().map(administrationMapper::toDTO).collect(Collectors.toList());
        return new ResponseEntity<>(actionDTOs, HttpStatus.OK);
    }
}
