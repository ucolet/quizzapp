package org.quizz.server.dto;

import lombok.Data;

@Data
public class ScoreDTO {
    public Long scoreId;
    public GameDTO game;
    public Long userId;
    public Long teamId;
    public int points;
    public int correctAnswers;
}
