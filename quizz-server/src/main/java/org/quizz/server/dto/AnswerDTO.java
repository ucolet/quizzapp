package org.quizz.server.dto;

import lombok.Data;

@Data
public class AnswerDTO {
    public Long answerId;
    public String answerText;
    public Boolean isCorrect;

    public AnswerDTO(int answerId, String answerText, boolean correct) {
        this.answerId = (long) answerId;
        this.answerText = answerText;
        this.isCorrect = correct;
    }
}
