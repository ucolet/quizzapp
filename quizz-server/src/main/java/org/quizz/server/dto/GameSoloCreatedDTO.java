package org.quizz.server.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.quizz.server.entity.Game;
import org.quizz.server.entity.Question;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class GameSoloCreatedDTO extends GameDTO {
    List<QuestionDTO> questions = new ArrayList<>();

    public GameSoloCreatedDTO(Game game, List<Question> questions) {
        this.gameId = game.getGameId();
        this.mode = game.getMode();
        this.difficulty = game.getDifficulty();
        this.theme = game.getTheme();
        this.questionCount = game.getQuestionCount();
        this.status = game.getStatus();
        questions.forEach(question -> {
            this.questions.add(new QuestionDTO(question));
        });
    }
}
