package org.quizz.server.dto;

import java.util.List;

import lombok.Data;
import org.quizz.server.entity.Question;

@Data
public class QuestionDTO {
    private int questionId;
    private String questionText;
    private String difficultyLevel;
    private String theme;
    private int points;
    private int penalty;
    private List<AnswerDTO> answers;

    public QuestionDTO(Question question) {
        this.questionId = question.getQuestionId();
        this.questionText = question.getQuestionText();
        this.difficultyLevel = question.getDifficultyLevel();
        this.theme = question.getTheme();
        this.points = question.getPoints();
        this.penalty = question.getPenalty();
        this.answers = question.getAnswers().stream()
                .map(answer -> new AnswerDTO(answer.getAnswerId(), answer.getAnswerText(), answer.isCorrect()))
                .toList();
    }
}

