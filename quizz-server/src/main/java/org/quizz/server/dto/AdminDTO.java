package org.quizz.server.dto;

import lombok.Data;

@Data
public class AdminDTO {
    public Long adminActionId;
    public Long adminId;
    public String action;
    public Long userId;
}
