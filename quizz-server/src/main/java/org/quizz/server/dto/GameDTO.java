package org.quizz.server.dto;

import lombok.Data;
import org.quizz.server.entity.Team;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class GameDTO {
    public int gameId;
    public int organizerId;
    public Date gameDate;
    public String mode;
    public String version;
    public String difficulty;
    public int questionCount;
    public String theme;
    public String status;
    public List<TeamDTO> teams = new ArrayList<>();
};
