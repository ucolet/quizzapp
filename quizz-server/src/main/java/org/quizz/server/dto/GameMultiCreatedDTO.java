package org.quizz.server.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.quizz.server.entity.Game;
import org.quizz.server.entity.Question;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class GameMultiCreatedDTO extends GameDTO{
    public List<QuestionDTO> teamAQuestions = new ArrayList<>();
    public List<QuestionDTO> teamBQuestions = new ArrayList<>();

    public GameMultiCreatedDTO(Game game, List<Question> teamAQuestions, List<Question> teamBQuestions) {
        this.gameId = game.getGameId();
        this.mode = game.getMode();
        this.difficulty = game.getDifficulty();
        this.theme = game.getTheme();
        this.questionCount = game.getQuestionCount();
        this.version = game.getVersion();
        this.status = game.getStatus();
        teamAQuestions.forEach(question -> {
            this.teamAQuestions.add(new QuestionDTO(question));
        });
        teamBQuestions.forEach(question -> {
            this.teamBQuestions.add(new QuestionDTO(question));
        });
        game.getTeams().forEach(team -> {
            this.teams.add(new TeamDTO(team.getTeamName()));
        });
    }
}
