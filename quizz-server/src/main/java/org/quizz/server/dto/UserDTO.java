package org.quizz.server.dto;

import lombok.Data;

@Data
public class UserDTO {
    public Long userId;
    public String username;
    public String profilePicture;
    public String role;
    public String accountStatus;
}
