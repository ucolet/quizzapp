package org.quizz.server.dto;

import lombok.Data;

@Data
public class ThemeDTO {
    public Long idTheme;
    public String name;
}
