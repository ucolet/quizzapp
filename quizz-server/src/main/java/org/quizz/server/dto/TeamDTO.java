package org.quizz.server.dto;

import lombok.Data;

@Data

public class TeamDTO {
    public String teamName;

    public TeamDTO(String teamName) {
        this.teamName = teamName;
    }

    public TeamDTO() {
    }
}
