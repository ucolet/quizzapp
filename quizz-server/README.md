# Serveur du projet de jeu de quizz

Ce projet est le serveur du projet de jeu de quizz. Il est développé en Java avec Spring Boot. Il inclut l'outil Liquibase pour la gestion des migrations de base de données.

## Configuration

Cf. [README.md](../README.md) global du projet.

## Avancement

- [x] Initialisation du projet
- [x] Mise en place de l'architecture (cf [Architecture](#architecture))
- [x] Mise en place de la connexion à la base de données
- [x] Mise en place de la connexion à l'API
- [x] Mise en place de la gestion des utilisateurs
- [x] Mise en place de la gestion des parties
- [x] Mise en place de la gestion des questions
- [x] Mise en place de la gestion des réponses
- [x] Mise en place de la gestion des scores
- [x] Mise en place de la gestion des classements

## Architecture

On a plusieurs dossiers importants dans le projet : 

- `src/main/java/org/quizz/server/config` : contient les configurations de l'application
- `src/main/java/org/quizz/server/controller` : contient les contrôleurs de l'application, c'est à dire la partie va gérer les requêtes et les réponses HTTP
- `src/main/java/org/quizz/server/entity` : contient les entités de l'application, c'est à dire les classes qui vont être mappées en tables dans la base de données
- `src/main/java/org/quizz/server/repository` : contient les interfaces de repository de l'application, c'est à dire les classes qui vont permettre de faire des requêtes en base de données
- `src/main/java/org/quizz/server/service` : contient les services de l'application, c'est à dire les classes qui vont encapsuler la logique métier de l'application
- `src/main/java/org/quizz/server/dto` : contient les classes dites DTO (Data Transfer Object) qui vont permettre de transférer des données entre l'API et le client
- `src/main/java/org/quizz/server/mapper` : contient les classes qui vont permettre de mapper des entités en DTO et vice versa
- `src/main/resources/db/changes` : contient les fichiers de migration Liquibase
