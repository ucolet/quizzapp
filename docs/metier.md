
# Gestion Client-Driven pour les Processus Métiers Complexes

## Introduction
Dans le contexte d'une application utilisant un **unique client**, plusieurs processus métiers peuvent être optimisés en externalisant une partie de la logique métier côté client. Cela permet de simplifier les responsabilités du backend et de réduire la charge serveur tout en maintenant une synchronisation efficace. De plus, le volume de données transmises entre le client et le serveur est faible dans le contexte de cette application, d'où ce choix-là.

---

## 1. Gestion des Parties en Mode "Équipe"

### Proposition
- Déplacer la gestion des tours, des équipes et des listes de questions côté client.
- Le backend agit comme point de synchronisation pour l'état global et les scores.

### Processus côté Client
1. **Initialisation** :
    - Le client envoie les paramètres au backend pour créer une partie et obtenir les questions.
2. **Gestion des Tours** :
    - La logique de rotation et de transitions est gérée localement au niveau du client.
3. **Synchronisation** :
    - Le client envoie l'état au backend à la fin de la partie.

---

## 2. Gestion des Scores et Pénalités

### Proposition
- Calcul des scores et des pénalités côté client.
- Le backend se concentre sur la validation et la persistance des scores finaux.

### Processus simplifié
1. **Calcul local** :
    - Le client applique les règles de score et pénalités directement.
2. **Mise à jour** :
    - Périodiquement, le client envoie une mise à jour globale :
      ```json
      {
        "teamA": { "score": 15 },
        "teamB": { "score": 12 }
      }
      ```
3. **Validation finale** :
    - À la fin de la partie, les scores globaux sont envoyés pour stockage.

---

## 3. Gestion des Timers et Expiration des Questions

### Proposition
- Gestion des timers localement avec synchronisation minimale au backend.

### Processus simplifié
1. **Timers côté client** :
    - Initialisation et expiration gérées en local via JavaScript.

---