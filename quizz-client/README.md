# Client du projet de jeu de quizz

Ce projet est le client du projet de jeu de quizz. Il est développé en React.js avec TypeScript.

## Configuration

Cf. [README.md](../README.md) global du projet.

## Avancement 

- [x] Initialisation du projet
- [x] Mise en place de l'architecture (cf [Architecture](#architecture))
- [x] Mise en place de la connexion à l'API avec Fetch API
- [] Mise en place de la page d'accueil
- [] Mise en place de la page de connexion
- [] Mise en place de la page d'inscription
- [x] Mise en place de la création d'une partie
- [x] Mise en place du jeu solo 
- [] Mise en place de la page de score
- [] Mise en place de la page de classement
- [] Mise en place de la page de profil
- [] Mise en place de la page d'administration (création de questions, réponses, gestion des users, etc.)

## Architecture

On a plusieurs dossiers importants dans le projet :

- `src/components` : contient les composants de l'application
- `src/pages` : contient les pages de l'application qui vont être rendues par le Router
- `src/services` : contient les services qui vont gérer les appels à l'API
- `src/hooks` : contient les hooks personnalisés, c'est-à-dire des fonctions qui vont être utilisées dans les composants pour gérer des états ou des effets
- `src/types` : contient les types TypeScript de l'application pour se référer au contrat de données exigées par notre API. On y trouve des enums et des types.



