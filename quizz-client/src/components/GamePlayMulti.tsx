import React, { useState, useEffect } from "react";
import { GameMultiCreated, Question, Answer } from "../types/Game";
import ActualQuestion from "./ActualQuestion";
import useTimer from "../hooks/useTimer";
import { GameVersion } from "../types/Enum";

interface GamePlayMultiProps {
    game: GameMultiCreated;
    onGameEnd: (winningTeam: string | null, scores: number[]) => void;
}

const GamePlayMulti: React.FC<GamePlayMultiProps> = ({ game, onGameEnd }) => {
    const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
    const [currentTeamIndex, setCurrentTeamIndex] = useState(0); // 0: Team A, 1: Team B
    const [scores, setScores] = useState<number[]>(Array(game.teams.length).fill(0)); // Scores for each team
    const [currentQuestions, setCurrentQuestions] = useState<Question[]>([]);
    const [lastAnswerCorrect, setLastAnswerCorrect] = useState<boolean | null>(null);

    let lastScoreState = [...scores]; // Tracks the most recent state of scores
    let teamAQuestionNumber: number = 0;
    let teamBQuestionNumber: number = 0;

    const { timeLeft, reset } = useTimer({
        initialTime: 10,
        onExpire: () => handleAnswerTimeout(),
    });

    // Initialize questions based on the game version
    useEffect(() => {
        if (game.version === GameVersion.V1) {
            const questions =
                currentTeamIndex === 0 ? game.teamAQuestions : game.teamBQuestions;
            setCurrentQuestions(questions || []);
        } else if (game.version === GameVersion.V2) {
            setCurrentQuestions([...game.teamAQuestions, ...game.teamBQuestions]);
            teamAQuestionNumber = game.teamAQuestions.length;
            teamBQuestionNumber = game.teamBQuestions.length;
        }
    }, [currentTeamIndex, game]);

    const handleAnswerSelect = (answer: Answer) => {
        const isCorrect = answer.isCorrect;
        setLastAnswerCorrect(isCorrect);

        if (isCorrect) {
            const updatedScores = [...scores];
            updatedScores[currentTeamIndex] += currentQuestions[currentQuestionIndex].points;
            setScores(updatedScores);
            lastScoreState = updatedScores; // Update lastScoreState with the new scores
            handleNextQuestion();
        } else {
            if (game.version === GameVersion.V1) {
                setTimeout(() => switchTeam(), 2000); // Switch team after an incorrect answer
            } else {
                handleNextQuestion(); // Continue to the next question in V2
            }
        }
    };

    const handleAnswerTimeout = () => {
        setLastAnswerCorrect(false);
        if (game.version === GameVersion.V1) {
            setTimeout(() => switchTeam(), 2000);
        } else {
            handleNextQuestion();
        }
    };

    const handleNextQuestion = () => {
        setLastAnswerCorrect(null);
        reset();

        if (currentQuestionIndex < currentQuestions.length - 1) {
            setCurrentQuestionIndex((prevIndex) => prevIndex + 1);
        } else if (game.version === GameVersion.V1) {
            // Check if other team still has questions
            if (currentTeamIndex === 0 && teamBQuestionNumber > 0) {
                teamAQuestionNumber = teamAQuestionNumber - 1;
                switchTeam();
            } else if (currentTeamIndex === 1 && teamAQuestionNumber > 0) {
                teamBQuestionNumber = teamBQuestionNumber - 1;
                switchTeam();
            } else {
                determineWinner();
            }
        } else {
            determineWinner();
        }
    };

    const switchTeam = () => {
        setLastAnswerCorrect(null);
        reset();
        setCurrentQuestionIndex(0); // Reset question index for the new team
        setCurrentTeamIndex((prevIndex) => (prevIndex + 1) % game.teams.length); // Switch team
    };

    const determineWinner = () => {
        const maxScore = Math.max(...lastScoreState);
        const winners = lastScoreState
            .map((score, index) => (score === maxScore ? game.teams[index].teamName : null))
            .filter((teamName) => teamName !== null);

        if (winners.length === 1) {
            onGameEnd(winners[0], lastScoreState); // One winning team
        } else {
            onGameEnd(null, lastScoreState); // Tie
        }
    };

    return (
        <div>
            <h2>Mode multi</h2>
            <p>Version: {game.version === GameVersion.V1 ? "Liste séparées" : "Liste partagée"}</p>
            <p>Scores:</p>
            <p>Tour de {game.teams[currentTeamIndex].teamName}</p>
            <ul>
                {game.teams.map((team, index) => (
                    <li key={team.teamName}>
                        {team.teamName}: {scores[index]}
                    </li>
                ))}
            </ul>
            {game.version === GameVersion.V1 && (
                <p>Current Team: {game.teams[currentTeamIndex].teamName}</p>
            )}
            <p>Time Remaining: {timeLeft}s</p>
            {currentQuestions.length > 0 && (
                <ActualQuestion
                    question={currentQuestions[currentQuestionIndex]}
                    onAnswerSelect={handleAnswerSelect}
                />
            )}
            {lastAnswerCorrect !== null && (
                <p style={{ color: lastAnswerCorrect ? "green" : "red" }}>
                    {lastAnswerCorrect ? "Correct Answer!" : "Incorrect Answer!"}
                </p>
            )}
        </div>
    );
};

export default GamePlayMulti;
