import React, { useState } from "react";
import { GameMultiCreated, GameSoloCreated, Team } from "../types/Game";
import { Difficulty, GameMode, GameVersion, Status, Theme } from "../types/Enum";
import { createMultiGame, createSoloGame } from "../services/gameService";

interface GameFormProps {
  onGameCreated: (game: GameSoloCreated | GameMultiCreated) => void;
}

const GameForm: React.FC<GameFormProps> = ({ onGameCreated }) => {
  const [formData, setFormData] = useState({
    mode: GameMode.SOLO,
    version: GameVersion.V1, // Utilisé uniquement en mode TEAM
    teams: [{teamName: "TeamA"} as Team, {teamName: "TeamB"} as Team], // Utilisé uniquement en mode TEAM
    date: new Date().toISOString(),
    difficulty: Difficulty.MEDIUM,
    theme: Theme.HISTORY,
    questionCount: 10,
    organizerId: 1,
    status: Status.PENDING,
  });
/**
  const handleChange = (e: React.ChangeEvent<HTMLSelectElement | HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };
*/
  const handleTeamsChange = (index: number, value: string) => {
    const updatedTeams = [...formData.teams];
    updatedTeams[index] = { ...updatedTeams[index], teamName: value };
    setFormData({ ...formData, teams: updatedTeams });
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    try {
      let game;
      if (formData.mode === GameMode.SOLO) {
         game = await createSoloGame(formData);
      } else {
        // Create multi game
        game = await createMultiGame(formData);
      }
      onGameCreated(game); // Pass the created game to the parent component
    } catch (error) {
      console.error("Error creating game:", error);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>Mode:</label>
      <select
        name="mode"
        value={formData.mode}
        onChange={(e) => setFormData({ ...formData, mode: e.target.value as GameMode })}
      >
        <option value={GameMode.SOLO}>Solo</option>
        <option value={GameMode.MULTI}>Équipe</option>
      </select>

      {formData.mode === GameMode.SOLO && (
        <>
          <label>Difficulté:</label>
          <select
            name="difficulty"
            value={formData.difficulty}
            onChange={(e) => setFormData({ ...formData, difficulty: e.target.value as Difficulty })}
          >
            {Object.values(Difficulty).map((difficulty) => (
              <option key={difficulty} value={difficulty}>
                {difficulty}
              </option>
            ))}
          </select>

          <label>Nombre de questions:</label>
          <input
            type="number"
            value={formData.questionCount}
            onChange={(e) =>
              setFormData({ ...formData, questionCount: parseInt(e.target.value) })
            }
            min="1"
          />

          <label>Thème:</label>
          <select
            name="theme"
            value={formData.theme}
            onChange={(e) => setFormData({ ...formData, theme: e.target.value as Theme })}
          >
            {Object.values(Theme).map((theme) => (
              <option key={theme} value={theme}>
                {theme}
              </option>
            ))}
          </select>
        </>
      )}

      {formData.mode === GameMode.MULTI && (
        <>
          <label>Version du jeu:</label>
          <select
            name="version"
            value={formData.version}
            onChange={(e) => setFormData({ ...formData, version: e.target.value as GameVersion })}
          >
            <option value="VERSION1">Version 1 (Deux listes)</option>
            <option value="VERSION2">Version 2 (Liste partagée)</option>
          </select>

          <label>Équipe A:</label>
          <input
            type="text"
            value={formData.teams[0].teamName}
            onChange={(e) => handleTeamsChange(0, e.target.value)}
          />

          <label>Équipe B:</label>
          <input
            type="text"
            value={formData.teams[1].teamName}
            onChange={(e) => handleTeamsChange(1, e.target.value)}
          />

          <label>Difficulté:</label>
          <select
            name="difficulty"
            value={formData.difficulty}
            onChange={(e) => setFormData({ ...formData, difficulty: e.target.value as Difficulty })}
          >
            {Object.values(Difficulty).map((difficulty) => (
              <option key={difficulty} value={difficulty}>
                {difficulty}
              </option>
            ))}
          </select>

          <label>Nombre de questions:</label>
          <input
            type="number"
            value={formData.questionCount}
            onChange={(e) =>
              setFormData({ ...formData, questionCount: parseInt(e.target.value) })
            }
            min="1"
          />

          <label>Thème:</label>
          <select
            name="theme"
            value={formData.theme}
            onChange={(e) => setFormData({ ...formData, theme: e.target.value as Theme })}
          >
            {Object.values(Theme).map((theme) => (
              <option key={theme} value={theme}>
                {theme}
              </option>
            ))}
          </select>
        </>
      )}

      <button type="submit">Créer la partie</button>
    </form>
  );
};

export default GameForm;
