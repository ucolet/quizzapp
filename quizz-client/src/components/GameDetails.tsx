import React from "react";
import { GameMultiCreated, GameSoloCreated } from "../types/Game";
import { GameMode } from "../types/Enum";

interface GameDetailsProps {
  game: GameSoloCreated | GameMultiCreated;
}

const GameDetails: React.FC<GameDetailsProps> = ({ game }) => {
  return (
    <div>
      <h3>Game Details</h3>
      <p>Game ID: {game.gameId}</p>
      <p>Mode: {game.mode}</p>
      <p>Difficulty: {game.difficulty}</p>
      <p>Theme: {game.theme}</p>
      <p>Status: {game.status}</p>
      <p>Organizer ID: {game.organizerId}</p>
      
      {game.mode === GameMode.MULTI && (
        <>
          <h4>Teams</h4>
          <ul>
            {game.teams.map((team) => (
                <p>Team Name: {team.teamName}</p>
            ))}
          </ul>
          <h4>Team A Questions</h4>
          <ul>
            {(game as GameMultiCreated).teamAQuestions.map((q) => (
              <li key={q.questionId}>
                <p>
                  {q.questionText} ({q.points} points, {q.penalty} penalty)
                </p>
                <ul>
                  {q.answers.map((a) => (
                    <li key={a.answerId}>
                      {a.answerText} {a.isCorrect ? "(Correct)" : ""}
                    </li>
                  ))}
                </ul>
              </li>
            ))}
          </ul>
          <h4>Team B Questions</h4>
          <ul>
            {(game as GameMultiCreated).teamBQuestions.map((q) => (
              <li key={q.questionId}>
                <p>
                  {q.questionText} ({q.points} points, {q.penalty} penalty)
                </p>
                <ul>
                  {q.answers.map((a) => (
                    <li key={a.answerId}>
                      {a.answerText} {a.isCorrect ? "(Correct)" : ""}
                    </li>
                  ))}
                </ul>
              </li>
            ))}
          </ul>
        </>
      )}

      {game.mode === GameMode.SOLO && (
        <>
          <h4>Questions</h4>
          <ul>
            {(game as GameSoloCreated).questions.map((q) => (
              <li key={q.questionId}>
                <p>
                  {q.questionText} ({q.points} points, {q.penalty} penalty)
                </p>
                <ul>
                  {q.answers.map((a) => (
                    <li key={a.answerId}>
                      {a.answerText} {a.isCorrect ? "(Correct)" : ""}
                    </li>
                  ))}
                </ul>
              </li>
            ))}
          </ul>
        </>
      )}
    </div>
  );
};

export default GameDetails;