import React from "react";
import { Question, Answer } from "../types/Game";

interface ActualQuestionProps {
  question: Question;
  onAnswerSelect: (answer: Answer) => void;
}

const ActualQuestion: React.FC<ActualQuestionProps> = ({ question, onAnswerSelect }) => {
  
    return (
    <div>
      <h3>{question.questionText}</h3>
      <ul>
        {question.answers.map((answer) => (
          <li key={answer.answerId}>
            <button onClick={() => onAnswerSelect(answer)}>{answer.answerText}</button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ActualQuestion;
