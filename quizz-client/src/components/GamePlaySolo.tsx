import React, { useState } from "react";
import { GameSoloCreated, Question, Answer, Game } from "../types/Game";
import ActualQuestion from "./ActualQuestion";
import useTimer from "../hooks/useTimer";

interface GamePlaySoloProps {
  game: GameSoloCreated;
  onGameEnd: (score: number) => void;
}

const GamePlaySolo: React.FC<GamePlaySoloProps> = ({ game, onGameEnd }) => {
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const [score, setScore] = useState(0);
  const [lastAnswerCorrect, setLastAnswerCorrect] = useState<boolean | null>(null);
  const currentQuestion: Question = game.questions[currentQuestionIndex];
  let lastScore = 0;

  const { timeLeft, reset } = useTimer({
    initialTime: 10,
    onExpire: () => handleAnswerTimeout(),
  });
  
  const handleAnswerSelect = (answer: Answer) => {
    const isCorrect = answer.isCorrect;
    setLastAnswerCorrect(isCorrect);


    if (isCorrect) {
      setScore((prevScore) => prevScore + currentQuestion.points);
      lastScore = score + currentQuestion.points;
    } else {
      setScore((prevScore) => Math.max(0, prevScore - currentQuestion.points));
      lastScore = Math.max(0, score - currentQuestion.points);
    }

    setTimeout(() => handleNextQuestion(), 2000);
  };

  const handleAnswerTimeout = () => {
    setLastAnswerCorrect(false);

    setTimeout(() => handleNextQuestion(), 2000);
  };

  const handleNextQuestion = () => {
    setLastAnswerCorrect(null); 
    reset();
    console.log(lastScore);

    if (currentQuestionIndex < game.questions.length - 1) {
      setCurrentQuestionIndex((prevIndex) => prevIndex + 1);
    } else {
      onGameEnd(lastScore);
    }
  };

  return (
    <div>
      <h2>Partie: {game.theme}</h2>
      <p>Score: {score}</p>
      <p>Temps restant: {timeLeft}s</p>
      <ActualQuestion question={currentQuestion} onAnswerSelect={handleAnswerSelect} />
      {lastAnswerCorrect !== null && (
        <p style={{ color: lastAnswerCorrect ? "green" : "red" }}>
          {lastAnswerCorrect ? "Bonne réponse !" : "Mauvaise réponse."}
        </p>
      )}
    </div>
  );
};

export default GamePlaySolo;
