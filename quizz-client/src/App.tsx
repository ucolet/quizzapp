import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { GamePage } from "./pages/GamePage";
import HomePage from "./pages/HomePage";
import NoFoundPage from "./pages/NoFoundPage.tsx";
import StatsPage from "./pages/StatsPage.tsx";


const App: React.FC = () => {
  return (
    <Router>
      <div>
        <Routes>
          {/* Route pour la page d'accueil */}
          <Route path="/" element={<HomePage />} />

          {/* Route pour la page de jeu */}
          <Route path="/game" element={<GamePage />} />

          {/* Route pour la page de stats */}
          <Route path="/stats" element={<StatsPage />} />

          {/* Route pour la page 404 */}
          <Route path="*" element={<NoFoundPage />} />


        </Routes>
      </div>
    </Router>
  );
};


export default App;
