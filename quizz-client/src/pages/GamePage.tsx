import { useState } from 'react';
import { GameSoloCreated, GameMultiCreated, Score} from '../types/Game';
import { createScore } from '../services/scoreService';
import GameForm from '../components/GameForm';
import GamePlay from '../components/GamePlaySolo';
import GameDetails from '../components/GameDetails';
import GamePlayMulti from '../components/GamePlayMulti';
import { GameMode } from '../types/Enum';


export const GamePage = () => {
    const [gameSolo, setGameSolo] = useState<GameSoloCreated | null>(null);
    const [gameMulti, setGameMulti] = useState<GameMultiCreated | null>(null);
    const [isPlaying, setIsPlaying] = useState(false);
    const [finalScore, setFinalScore] = useState<number | null>(null);
  
    // Fonction pour réinitialiser tous les états
    const resetGame = () => {
      setGameSolo(null);
      setGameMulti(null);
      setIsPlaying(false);
      setFinalScore(null);
    };
  
    const handleGameCreated = (createdGame: GameSoloCreated | GameMultiCreated) => {
      resetGame(); // Réinitialise l'état avant de définir la nouvelle partie
      if (createdGame.mode === GameMode.SOLO) {
        setGameSolo(createdGame as GameSoloCreated);
      } else if (createdGame.mode === GameMode.MULTI) {
        setGameMulti(createdGame as GameMultiCreated);
      }
    };
  
    const handleStartGame = () => {
      setIsPlaying(true);
    };
  
    const handleGameEndSolo = (score: number) => {
      setFinalScore(score);
      setIsPlaying(false);
  
      if (score > 0) {
        const victorySound = new Audio('/sounds/victory.mp3');
        victorySound.play();
      } else {
        const defeatSound = new Audio('/sounds/defeat.mp3');
        defeatSound.play();
      }
  
      handleScoreSubmit(score);
    };
    
  
    const handleGameEndMulti = (winningTeam: string | null, scores: number[]) => {
      setFinalScore(scores[0] > scores[1] ? scores[0] : scores[1]);
      setIsPlaying(false);
      const winningMessage = winningTeam
        ? `Winning Team: ${winningTeam}`
        : "It's a tie!";
      alert(winningMessage);
      console.log("Final scores:", scores);
      scores.forEach((score) => handleScoreSubmit(score));
    };
  
    const handleScoreSubmit = async (score: number) => {
      if (gameSolo && score) {
        const data: Partial<Score> = {
          game: gameSolo,
          points: score,
          correctAnswers: score / 100, // Exemple de calcul des réponses correctes
        };
        console.log("data", data);
        // Envoi du score au serveur
        await createScore(data);
      } else if (gameMulti && score !== null) {
        // Envoi du score au serveur
        console.log("Multiplayer score:", score);
        const data: Partial<Score> = {
          game: gameMulti,
          points: score,
          correctAnswers: score / 100,
        };
        await createScore(data);
      }
    };
  
    return (
      <div>
        <h1>Quiz Game</h1>
        {!gameSolo && !gameMulti ? (
          <GameForm onGameCreated={handleGameCreated} />
        ) : isPlaying ? (
          gameSolo ? (
            <GamePlay game={gameSolo} onGameEnd={handleGameEndSolo} />
          ) : (
            <GamePlayMulti game={gameMulti!} onGameEnd={handleGameEndMulti} />
          )
        ) : finalScore !== null ? (
          <div>
            <h2>Game Over</h2>
            <p>Score final: {finalScore}</p>
            <button onClick={resetGame}>Restart</button>
          </div>
        ) : (
          <div>
            {gameSolo && <GameDetails game={gameSolo} />}
            {gameMulti && <GameDetails game={gameMulti} />}
            <button onClick={handleStartGame}>Start Game</button>
            <button onClick={resetGame}>Create a New Game</button>
          </div>
        )}
      </div>
    );
  };