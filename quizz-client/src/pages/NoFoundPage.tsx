import React from "react";
import { Link } from "react-router-dom";

const NoFoundPage: React.FC = () => {
    return (
        <div>
            <h1>Erreur 404</h1>
            <p>La page que vous recherchez n'existe pas ou a été déplacée.</p>
            <Link to="/">Retour à l'accueil</Link>
        </div>
    )
};

export default NoFoundPage;