import React, { useEffect, useState } from "react";
import { fetchQuestionsWithAnswers } from "../services/questionService";
import { Question } from "../types/Game";

const AdminPage: React.FC = () => {
  const [questions, setQuestions] = useState<Question[]>([]); // Type explicite ici

  useEffect(() => {
    const loadQuestions = async () => {
      try {
        const fetchedQuestions = await fetchQuestionsWithAnswers();
        setQuestions(fetchedQuestions);
      } catch (error) {
        console.error("Erreur lors du chargement des questions :", error);
      }
    };
    loadQuestions();
  }, []);

  return (
    <div>
      <h1>Admin Panel</h1>
      <h2>Questions</h2>
      <table>
        <thead>
        <tr>
          <th>ID</th>
          <th>Texte</th>
          <th>Difficulté</th>
          <th>Thème</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        {questions.map((question) => (
          <tr key={question.questionId}>
            <td>{question.questionId}</td>
            <td>{question.questionText}</td>
            <td>{question.difficultyLevel}</td>
            <td>{question.theme}</td>
            <td>
              <button>Modifier</button>
              <button>Supprimer</button>
            </td>
          </tr>
        ))}
        </tbody>
      </table>
    </div>
  );
};

export default AdminPage;
