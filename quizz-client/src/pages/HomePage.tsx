import React from "react";
import { useNavigate } from "react-router-dom";

const Homepage: React.FC = () => {
  const navigate = useNavigate();

  const handleStartGame = () => {
    navigate("/game"); // Redirige vers la page de jeu
  };

  const handleStats= () => {
    navigate("/stats"); // Redirige vers la page de stats
  };

  return (
    <div>
      <h2>Bienvenue sur Quiz Game</h2>
      <button onClick={handleStats}>Voir mes stats</button>
      <p>Choisissez un mode de jeu pour commencer.</p>
      <button onClick={handleStartGame}>Commencer une partie</button>
    </div>

  );
};

export default Homepage;
