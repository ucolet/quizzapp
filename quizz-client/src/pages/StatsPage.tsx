import React, { useEffect, useState } from 'react';
import { ScoreService } from '../services/statsService.ts'; // Import the service

const ScoresPage: React.FC = () => {
  const [scores, setScores] = useState<string[]>([]); // Now using string[] since it's an array of strings
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  // Fetch scores when the component mounts
  useEffect(() => {
    const loadScores = async () => {
      try {
        const data = await ScoreService.fetchScores(); // Fetching an array of strings
        setScores(data); // data is now an array of strings
      } catch (err) {
        setError('Failed to load scores');
      } finally {
        setLoading(false);
      }
    };

    loadScores();
  }, []); // Empty dependency array means this runs only once when the component mounts

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div>
      <h1>Scores</h1>
      <ul>
        {scores.map((score, index) => (
          <li key={index}>{score}</li>
          ))}
      </ul>
    </div>
  );
};

export default ScoresPage;