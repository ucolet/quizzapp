import { Difficulty, GameMode, GameVersion, Status, Theme } from "./Enum";

export interface Answer {
  answerId: number;
  answerText: string;
  isCorrect: boolean;
}

export interface Question {
  questionId: number;
  questionText: string;
  difficultyLevel: Difficulty;
  theme: Theme;
  points: number;
  penalty: number;
  answers: Answer[];
}

export interface Game {
  gameId?: number;
  organizerId: number;
  gameDate: Date;
  mode: GameMode;
  version: GameVersion;
  difficulty: Difficulty;
  questionCount: number;
  theme: Theme;
  status: Status;
  teams: Team[];
}

export interface GameSoloCreated extends Game {
  questions: Question[];
}

export interface GameMultiCreated extends Game {
  teamAQuestions: Question[];
  teamBQuestions: Question[];
}

export interface Score {
  game: Game;
  userId?: number;
  teamId?: number;
  points: number;
  correctAnswers: number;
}

export interface Team {
  teamName: string;
}