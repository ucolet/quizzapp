export enum Difficulty {
    EASY = "EASY",
    MEDIUM = "MEDIUM",
    HARD = "HARD"
}

export enum Theme {
    HISTORY = "HISTORY",
    SPORTS = "SPORTS",
    MUSIC = "MUSIC",
    LITTERATURE = "LITTERATURE",
}

export enum Status {
    PENDING = "PENDING",
    ONGOING = "ONGOING",
    COMPLETED = "COMPLETED"
}

export enum GameMode {
    SOLO = "SOLO",
    MULTI = "MULTI",
}

export enum GameVersion {
    V1 = "VERSION1",
    V2 = "VERSION2"
}