import { Score } from "../types/Game";
import { apiFetch } from "./api";

export const createScore = async (data: Partial<Score>): Promise<Score> => {
    return apiFetch("/scores", {
        method: "POST",
        body: JSON.stringify(data),
    });
};

export const getScores = async (gameId: number): Promise<Score[]> => {
    return apiFetch(`/scores/${gameId}`);
}
