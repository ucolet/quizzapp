import {apiFetch} from "./api";
import { Score } from '../types/Game'; // Import the Score type

export class ScoreService {
  private static API_URL = './api.ts'; // Adjust to your actual API endpoint

  // Method to fetch scores and use the custom toString method
  static async fetchScores(): Promise<string[]> {
    try {
      const data: Score[] = await apiFetch(this.API_URL); // Assuming apiFetch returns a Score[] array

      // Using the built-in toString method on the array
      return data.toString().split(","); // Convert the array to a string and split by commas
    } catch (error) {
      console.error("Error fetching scores:", error);
      throw new Error('Failed to fetch scores');
    }
  }
}