const BASE_URL = import.meta.env.VITE_API_URL || "http://localhost:8081/api";

export const apiFetch = async (endpoint: string, options: RequestInit = {}) => {
  const defaultOptions: RequestInit = {
    headers: {
      "Content-Type": "application/json",
    },
    credentials: "include", // Include cookies if needed
    ...options,
  };

  const response = await fetch(`${BASE_URL}${endpoint}`, defaultOptions);

  if (!response.ok) {
    const errorData = await response.json();
    throw new Error(errorData.message || "Something went wrong");
  }

  return response.json();
};

const api = { fetch: apiFetch };
export default api;