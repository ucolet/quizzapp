import { apiFetch } from "./api"; // Import your fetch utility
import { Game, GameMultiCreated, GameSoloCreated } from "../types/Game";

// Create a new game (returns GameSoloCreated including questions

export const createSoloGame = async (data: Partial<Game>): Promise<GameSoloCreated> => {
  return apiFetch("/games/solo", {
    method: "POST",
    body: JSON.stringify(data),
  });
}

export const createMultiGame = async (data: Partial<Game>): Promise<GameMultiCreated> => {
  return apiFetch("/games/multi", {
    method: "POST",
    body: JSON.stringify(data),
  });
}

// Fetch detailed information about a game
export const fetchGameDetails = async (gameId: number): Promise<GameSoloCreated> => {
  return apiFetch(`/games/${gameId}`);
};

// Update the game state
export const updateGameState = async (gameId: number, state: Partial<Game>): Promise<Game> => {
  return apiFetch(`/games/${gameId}`, {
    method: "PATCH",
    body: JSON.stringify(state),
  });
};
