import api from "./api";
import { Question } from "../types/Game";

// Fetch questions (includes nested answers)
export const fetchQuestionsWithAnswers = async (): Promise<Question[]> => {
  const response = await api.fetch("/questions");
  return response.data;
};

