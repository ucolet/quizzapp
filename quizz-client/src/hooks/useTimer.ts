import { useState, useEffect } from "react";

interface UseTimerOptions {
  initialTime: number;
  onExpire?: () => void; // Callback déclenché lorsque le temps expire
}

const useTimer = ({ initialTime, onExpire }: UseTimerOptions) => {
  const [timeLeft, setTimeLeft] = useState(initialTime);

  useEffect(() => {
    if (timeLeft <= 0) {
      if (onExpire) onExpire();
      return;
    }

    const timer = setInterval(() => {
      setTimeLeft((prevTime) => Math.max(prevTime - 1, 0));
    }, 1000);

    return () => clearInterval(timer);
  }, [timeLeft, onExpire]);

  const reset = (newTime: number = initialTime) => {
    setTimeLeft(newTime);
  };

  return { timeLeft, reset };
};

export default useTimer;
